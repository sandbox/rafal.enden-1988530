Description
Normally Blockify module support only primary tabs as block, this module changes that, and provide block also for secondary tabs.

Requirements
1. Blockify module (http://drupal.org/project/blockify).

Installation
1. Unpack and move directory "blockify_secondary_tabs" to your modules directory.
2. Enable it in the modules list of your site.
3. Move block "Secondary tabs" in region of your choice.
